/*###ICF### Section handled by ICF editor, don't touch! ****/
/*-Editor annotation file-*/
/* IcfEditorFile="$TOOLKIT_DIR$\config\ide\IcfEditor\a_v1_0.xml" */


/*****************************************************************************/
/*** FLASH ***/
/*****************************************************************************/

//SRAM



//   | ---------------	 ---------------  _region_SRAM_CODE_start__
//   |   		|		|
//   |   		|		|
//   |   		|		|
//   |   		|		|
//   |   		|   SRAM_CODE	|
//   |   		|		|
//   |   		|		|
//   |   		|		|
//   |   		|		|
//   |   		| --------------|_region_SRAM_CODE_start__ + _region_SRAM_CODE_size__
//   |   		| --------------|_region_SRAM_CODE_start__ + _region_MAX_CODE_size__
//   |   SRAM_BASE	| --------------|_region_SRAM_RW_start__
//   |   		|		|
//   |   		|		|
//   |   		|		|
//   |   		|    SRAM_RW	|
//   |   		|		|
//   |   		|		|
//   |   		|		|
//   |   		|		|
//   |   		|		|
//   |   		| --------------|_region_SRAM_RW_start__ + _region_SRAM_RW_size__
//    ---------------	| --------------|_region_SRAM_RW_start__ + _region_SRAM_BASE_size__-_region_MAX_CODE_size__
//			| --------------|_region_SRAM_EXT_start__
//			|		|
//			|		|
//			|		|
//			|		|
//			|		|
//			|    SRAM_EXT	|
//			|		|
//			|		|
//			|		|
//			| --------------|_region_SRAM_EXT_start__ + _region_SRAM_EXT_size__
//			  -------------- _region_SRAM_EXT_start__ + _region_SRAM_size__-_region_SRAM_BASE_size__



// Capella default config: DTCM ON / ITCM OFF

define symbol wait_loop_offset = 0x200;

/*****************************************************************************/
/* - Memory Regions - */
define symbol _region_FLASH_start__ = 0x00400000;
define symbol _region_FLASH_size__  = 0x00080000;  //512 KB minimal config. Product up to 1MB = 0x100000
/**********************************************************/
define symbol _ITCM_cfg__ = 0x0;
export symbol _ITCM_cfg__;
define symbol _DTCM_cfg__ = 0x1;
export symbol _DTCM_cfg__;
define symbol _region_SRAM_CODE_start__ = 0x1FFE0000;
define symbol _region_SRAM_CODE_size__  = 0x00020000;   //128KB
define symbol _region_DTCM_start__ = 0x20000000;
define symbol _region_DTCM_size__  = 0x00040000;   //256KB
define symbol _region_ITCM_start__ = 0x00000000;
define symbol _region_ITCM_size__  = 0x00000000;   //0KB

define symbol _region_SDRAM_start__ = 0x70000000;

/**********************************************************/
define symbol _region_SRAM_BASE_size__ = 0x00058000; //352KB=128KBcode +224KBrw
export symbol _region_SRAM_BASE_size__;
/**********************************************************/
define symbol _region_MAX_CODE_size__  = 0x0000EFE0;
export symbol _region_MAX_CODE_size__;


/**********************************************************/
define symbol _region_SRAM_BASE_start__ = _region_SRAM_CODE_start__;

define symbol _region_SRAM_RW_start__   = _region_SRAM_CODE_start__+_region_SRAM_CODE_size__;

define symbol _region_SRAM_RW_size__    = _region_SRAM_BASE_size__-_region_SRAM_CODE_size__;
define symbol _region_SRAM_EXT_start__  = _region_SRAM_RW_start__+_region_SRAM_RW_size__;
define symbol _region_SRAM_EXT_size__   = _region_DTCM_size__-_region_SRAM_RW_size__;
define symbol _region_FLASH_CODE_size__  = _region_MAX_CODE_size__;
define symbol _region_FLASH_CODE_start__ = _region_FLASH_start__;

export symbol _region_SRAM_EXT_start__;
export symbol _region_SRAM_EXT_size__;

define memory mem with size = 4G;
define region FLASH  = mem:[from _region_FLASH_start__  size _region_FLASH_size__];

//define region SRAM  = mem:[from _region_SRAM_start__  size _region_SRAM_size__];
define region SRAM_CODE = mem:[from _region_SRAM_CODE_start__  size _region_SRAM_CODE_size__];
define region SRAM_RW   = mem:[from _region_SRAM_RW_start__  size _region_SRAM_RW_size__];
define region SRAM_BASE = mem:[from _region_SRAM_BASE_start__  size _region_SRAM_BASE_size__];
define region SRAM_EXT  = mem:[from _region_SRAM_EXT_start__ size _region_SRAM_EXT_size__];
define region FLASH_CODE = mem:[from _region_FLASH_CODE_start__ size _region_FLASH_CODE_size__];
/*****************************************************************************/


/*-binary infos-*/
define symbol _info_add__ = _region_FLASH_CODE_start__+_region_FLASH_CODE_size__;
place at address mem:_info_add__ { ro section .info_section };

/*-checksum-*/
define symbol _checksum_add__ = _region_FLASH_CODE_start__+_region_FLASH_CODE_size__+0x1C;
export symbol _checksum_add__;
place at address mem:_checksum_add__ { ro section .checksum };

/*-Stack-*/
define symbol __ICFEDIT_size_cstack__   = 0x1400;
define symbol __ICFEDIT_size_heap__     = 0x0200;
define block CSTACK    with alignment = 4, size = __ICFEDIT_size_cstack__   { };
define block HEAP      with alignment = 4, size = __ICFEDIT_size_heap__     { };


/*-Specials-*/
define symbol __ICFEDIT_intvec_start__ = _region_FLASH_start__;
define symbol __ICFEDIT_test_start__ = _region_FLASH_start__+wait_loop_offset;
define symbol __ICFEDIT_rwitcm_start__ = _region_SRAM_CODE_start__+_region_MAX_CODE_size__;
define symbol __ICFEDIT_rwsdram_start__ = _region_SDRAM_start__;

initialize by copy { rw };
do not initialize  { section .noinit };

place at address mem:__ICFEDIT_intvec_start__ { ro section .intvec };
place at address mem:__ICFEDIT_test_start__ { ro section .wait_time };
place at address mem:__ICFEDIT_rwitcm_start__ { rw section .rw_itcm_area};
place at address mem:__ICFEDIT_rwsdram_start__ { rw section .rw_sdram_area};


place in FLASH_CODE  { ro  };
place in SRAM_RW     { rw , block CSTACK };
place in SRAM_EXT    { block HEAP };
                       
